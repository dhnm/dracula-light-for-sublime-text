# Dracula Light for Sublime Text 3 & TextMate 2

Made using [Light Dracula Generator][https://gitlab.com/dhnm/light-dracula-generator].

## Instructions

1. Download the files.
2. For TextMate 2 double-click the themes, select *Themes* Bundle. Then go to `View > Theme > Theme for Light Appearance`. Your theme(s) will be below the default themes.
3. Follow instructions on how to manually install themes for Sublime Text, like [this one][https://colorsublime.github.io/how-to-install-a-theme/].